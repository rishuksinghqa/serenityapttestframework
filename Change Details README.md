================== Change Details ===========
1. Removed folders/files related to gradle form the project directory
2. Refactored *post_product.feature* Feature file
  - Added Scenario Heading
  - Created 5 scenarios(4 positive scenarios for different products search)and one negative scenario.
  - Replaced step *Then he sees the results displayed for apple* with *Then he sees the results displayed for "apple"*
  - Create this step to reuse the same step to verify product search for all the products with passing different products name
  - Created steps to validate all the products

3. Changes in SearchStepDefinitions class
  - Created new setdefinition method heSeesTheResultsDisplayedFor and added assertion to verify status code and product name
  -Fixed method heDoesnNotSeeTheResults
    - Added code to assert status code in case of invalid product
    - Added code to assert error and error description

4. Renamed html report name in serenity.properties file

*Reason for 4 test scripts failure*
- For some of the Products product name is not present in response
 *Example* -
  *Expected* - Apple
  *Actual* - Jumbo Jonagold Extra Voordeel 1

*Expected* - mango
 *Actual* - 8% Fat Yogurt 200 g

*Refer target\site\serenity\index.html report for more details*
