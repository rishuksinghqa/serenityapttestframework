Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios
  @Positive
  Scenario: Search for product apple
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/apple"
    Then he sees the results displayed for "apple"
  @Positive
  Scenario: Search for product mango
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/mango"
    Then he sees the results displayed for "mango"
  @Positive
  Scenario: Search for product tofu
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/tofu"
    Then he sees the results displayed for "tofu"
  @Positive
  Scenario: Search for product water
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/water"
    Then he sees the results displayed for "water"
  @Negative
  Scenario: Search product with invalid product name
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/car"
    Then he doesn not see the results
